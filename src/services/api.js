import axios from 'axios';

const api = axios.create({ baseURL: 'http://localhost:3001/api' });

// Método para definir se faz cadastro ou edição no backend
// Caso receba um ID faz PUT para atualizar, caso o ID seja nulo, faz um POST para cadastro
api.postOrPut = (url, id, data, config = {}) => {
    const method = id ? 'put' : 'post';
    const apiUrl = id ? `${url}/${id}` : url;
    return api[method](apiUrl, data, config);
};

export default api;