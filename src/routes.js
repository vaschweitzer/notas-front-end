import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';  //Switch faz com que apenas uma rotas seja chamada por vez
import Notas from './pages/notas'
import Nota from './pages/nota'


// Necessário o exact, se não ele irá entrar sempre no path / que é o primeiro encontrado no endereço submetido
const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Notas} />
            <Route exact path="/notas" component={Notas} />
            <Route exact path="/notas/edit" component={Nota} />
        </Switch>
    </BrowserRouter>
);

export default Routes;