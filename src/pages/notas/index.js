import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { Button, List, ListItem, IconButton, ListItemSecondaryAction } from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';

import api from "../../services/api";
import "./styles.css";


export default class Notas extends Component {

    state = {
        notas: [],
        dataInfo: {}, //metadados para paginação
        page: 1         //página atual
    };

    // Carrega notas assim que o componente é montado
    componentDidMount() {
        this.carregaNotas();
    }

    /* Método que carrega as notas que estão no banco para mostrar na tela.
       Solicita os dados para a api, que faz a consulto no backend
    */
    carregaNotas = async (page = 1) => {
        const response = await api.get(`/notas?page=${page}`);
        const { docs, ...dataInfo } = response.data; // ... significa 'o resto além do docs', uso de desestruturação
        this.setState({ notas: docs, dataInfo, page });
    };

    // Método para apagar uma nota
    deletaNota = async (id_nota) => {
        const response = await api.delete(`/notas/${id_nota}`);
        this.carregaNotas();
    };

    // Método para para voltar uma página
    prevPage = () => {
        const { page } = this.state;
        if (page === 1) return; // se for a primeira página, não faz nada
        const pageNumber = page - 1;
        this.carregaNotas(pageNumber);
    }

    // Método para para passar uma página
    nextPage = () => {
        const { page, dataInfo } = this.state;

        if (page === dataInfo.pages) return;  // se for a última página, não faz nada

        const pageNumber = page + 1;
        this.carregaNotas(pageNumber);
    }

    // Método que renderiza a página (executado a cada alteração do estado do componente)
    render() {
        const { notas, page, dataInfo } = this.state;
        return (
            <div className={"notas"}>
                <div className="listaNotas">
                    <List >
                        {notas.map(nota => (
                            <article key={nota._id}>
                                <ListItem>
                                    <strong>{nota.titulo}</strong>
                                    <ListItemSecondaryAction>
                                        <IconButton edge="end" aria-label="edit" component={Link} to={{ pathname: '/notas/edit', state: { nota } }}>
                                            <EditIcon />
                                        </IconButton>
                                        <IconButton edge="end" aria-label="cancel" onClick={() => { this.deletaNota(nota._id); }} >
                                            <DeleteIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                                <pre>{nota.descricao}</pre>
                            </article>
                        ))}
                    </List>
                </div>
                <div className="botoes_navegacao">
                    <Button id="btAnterior" variant="contained" color="primary" aria-label="prev" disabled={page === 1} onClick={this.prevPage}>Anterior</Button>
                    <Button variant="contained" color="primary" aria-label="next" disabled={page === dataInfo.pages} onClick={this.nextPage}>Próxima</Button>
                    <div className="botoes_funcao">
                        <Button variant="contained" color="primary" aria-label="add" component={Link} to={{ pathname: '/notas/edit' }}>
                            Adicionar nota
                            <AddIcon className={'rightIcon'} />
                        </Button>
                    </div>
                </div>
            </div >
        );

    }



}