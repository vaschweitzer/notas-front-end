import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import CancelIcon from '@material-ui/icons/Cancel';
import "./styles.css";
import api from "../../services/api";
import { Link, Redirect } from 'react-router-dom';


export default class Nota extends Component {

    state = {
        id: '',
        titulo: '',
        descricao: '',
        alteradoEm: '',
        notaEnviada: false,
    };

    constructor(props) {
        super(props);
    };


    /* Inicia o componente para tela de cadastro/edição.
       Se o componente receber dados de uma nota como props, irá iniciar os campos com estes dados, para que a nota seja editada.
       Se o componente não receber parâmetros para ser construído, irá inciar com os campos em branco, para um noco cadastro.
    */
    componentDidMount() {
        try {
            this.setState({ id: this.props.location.state.nota._id, titulo: this.props.location.state.nota.titulo, descricao: this.props.location.state.nota.descricao });
        } catch (e) {
            this.setState({ id: '', titulo: '', descricao: '' });
        }
    }

    // Método para atualizar o estado a cada vez que os campos de título ou descrição forem alterados
    myChangeHandler = (event) => {
        let nomeCampo = event.target.name;
        let valorCampo = event.target.value;
        this.setState({ [nomeCampo]: valorCampo, alteradoEm: Date.now() });
    }

    // Método executado quando o form é submetido. Cancela o evento de link e executa o método salvarNota
    mySubmitHandler = (event) => {
        console.log(this.state.alteradoEm)
        event.preventDefault();
        this.salvarNota();
    };

    // Método que aciona a api para salvar a nota cadastrada/editada
    salvarNota = async () => {
        const { id } = this.state;
        try {
            const response = await api.postOrPut(`/notas`, id, this.state);
            this.setState({ notaEnviada: true })
        } catch (e) {
            console.log('Erro ao enviar dados para o banco de dados');
        }
    }

    // Método que renderiza a página (executado a cada alteração do estado d ocomponente)
    render() {

        let { titulo, descricao } = this.state;

        if (this.state.notaEnviada === true) {
            return (<Redirect to="/notas" />);
        } else

            return (

                <form className="containerForm" onSubmit={this.mySubmitHandler}>

                    <TextField
                        id="tituloTextArea"
                        className="textField"
                        name="titulo"
                        label="Título"
                        placeholder="Título de sua tarefa"
                        onChange={this.myChangeHandler}
                        value={titulo}

                        fullWidth
                        required  // error  disabled
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />

                    <TextField
                        id="descricaoTextArea"
                        className="textField"
                        name="descricao"
                        label="Nota"
                        placeholder="Detalhes de sua tarefa"
                        onChange={this.myChangeHandler}
                        value={descricao}

                        fullWidth
                        multiline
                        rows="10"
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <div className="divBotoes">
                        <Button className="botaoAcao" variant="contained" color="primary" type='submit' >
                            Enviar
                    < SendIcon className="rightIcon" />
                        </Button>
                        <Button className="botaoAcao" variant="contained" color="secondary" component={Link} to={{ pathname: '/notas' }} >
                            Cancelar
                    <CancelIcon className="rightIcon" />
                        </Button>
                    </div>

                </form>

            );
    }
}