Aplicação web exemplo para registro de tarefas.

Desenvolvida com a biblioteca React.js


## Requisitos
- Estar com o servidor back end rodando no servidor.
- Link do projeto back end: https://gitlab.com/vaschweitzer/notas-back-end

## Passos para execução do projeto

- Clonar o projeto GIT ou fazer o download do mesmo em sua máquina local;
- Entrar na pasta principal do projeto;
- Executar "npm install" para baixar as bibliotecas utilizadas e suas dependências;
- Executar "npm start";
- Acessar através de um navegador o endereço: http://localhost:3000
